# Git Demo

The purpose of the repository is to familiarize members with Git practices

Please follow the steps outline on [this Confluence page](https://kafkadev.atlassian.net/wiki/spaces/SA/pages/2523146/Git+Workflow)


## Team members

* Kahlil Wehmeyer